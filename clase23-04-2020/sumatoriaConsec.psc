Proceso sumatoriaConsec
//	Escriba un programa que pida al usuario dos n�meros enteros, y luego entregue la suma de todos
//	los n�meros que est�n entre ellos. Por ejemplo, si los n�meros son 1 y 7, debe entregar como
//		resultado 2 + 3 + 4 + 5 + 6 = 20.
	Definir i,ac,start,end como entero;
	ac <- 0;
	Escribir "Este programa calcula la sumatoria consecutuva dado un rango.";
	Escribir "Empezar con:";
	Leer start;
	Escribir  "Terminar con:";
	Leer end;
	si start < end Entonces
		Para i <- start+1 Hasta end-1 Con Paso 1 Hacer
			ac <- ac + i;
		FinPara
		Escribir "La sumatoria consecutiva desde ", start, " hasta ",end, " es ", ac,".";
	SiNo
		Escribir  "Rango no valido.";
	FinSi
FinProceso
