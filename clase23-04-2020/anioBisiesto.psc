Proceso anioBiciesto
//1 � Escriba un programa que indique si un a�o es bisiesto o no, teniendo en cuenta lo siguiente:
//		� Es bisiesto si es divisible entre 4
//		� Pero no es bisiesto si es divisible entre 100
//		� Pero si es bisiesto si es divisible entre 400	
	Definir a como entero;
	Definir d4,d100,d400 como Logico;
	Escribir "El siguiente programa determina si un a�o es bisiesto.";
	Escribir "Engrese a�o:";	
	Leer a;
	d4 <- a%4 = 0;
	d100 <- a%100 = 0;
	d400 <- a%400 = 0;
	si d400 | (d4 & !d100) Entonces
		Escribir "El a�o ",a," es bisiesto.";
	SiNo
		Escribir "El a�o ",a," no es bisiesto.";
	FinSi
FinProceso
