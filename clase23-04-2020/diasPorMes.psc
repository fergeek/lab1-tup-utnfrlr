//	5 - Escriba un programa que introduzca un mes (1-12) y visualice el numero de d�as de ese mes,
//	para el caso de febrero pedir al usuario el a�o para determinar la cantidad de d�as.

Funcion ret <- ES_BISIESTO(a)
	Definir d4,d100,d400,ret como Logico;
	d4 <- a%4=0;
	d100 <- a%100 = 0;
	d400 <- a%400 = 0;
	ret <- d400 | (d4 & !d100);
FinFuncion

Funcion ret <- MES_NOMBRE(m)
	Definir nomMes,ret como cadena;
	Dimension nomMes[12];
	nomMes[0] <- "Enero";
	nomMes[1] <- "Febrero";
	nomMes[2] <- "Marzo";
	nomMes[3] <- "Abril";
	nomMes[4] <- "Mayo";
	nomMes[5] <- "Junio";
	nomMes[6] <- "Julio";
	nomMes[7] <- "Agosto";
	nomMes[8] <- "Septiembre";
	nomMes[9] <- "Octubre";
	nomMes[10] <- "Noviembre";
	nomMes[11] <- "Diciembre";
	si	m>-1 & m <13 entonces
		ret <- nomMes[m-1];
	SiNo
		ret <- "Undefined";
	FinSi
FinFuncion

Proceso diasTotales
	Definir mes,a,dias como Entero;
	Escribir "Ingrese a�o:";
	Leer a;
	Escribir "Ingrese mes:";
	Leer mes;
	Segun mes Hacer
		1 3 4 7 8 10 12: dias <- 31;
		5 6 9 11: dias <- 30;
		2: dias <- 28;
		De Otro Modo: dias <- -1;
	FinSegun
	si dias != -1 Entonces
		si ES_BISIESTO(a) & mes = 2 entonces 
			dias <- dias + 1;
		FinSi
		Escribir MES_NOMBRE(mes)," (",mes,") del ",a," tiene ",dias," dias.";
	SiNo
		Escribir  "El mes no es valido.";
	FinSi
FinProceso
//Meses con 30 d�as: Abril, Junio, Septiembre y Noviembre.
//Meses con 31 d�as: Enero, Marzo, Mayo, Julio, Agosto, Octubre y Diciembre.
//Meses con 28 d�as: Febrero (Menos cuando es bisiesto que tiene 29 d�as).