Proceso calificacionAlfabetica
	// 2 � Empleando la estructura de control �seg�n�, escriba un programa que introduzca una
	// calificaci�n de A a F y emita el mensaje correspondiente de acuerdo a la nota ingresada:
	// A- Excelente
	// B- Buena
	// C- Regular
	// D- Suficiente
	// F- No suficiente
	Definir ca,verif,res Como Cadena;
	Definir p,i como Entero;
	Dimension verif[5];
	p <- (-1);
	verif[0] <- 'A';
	verif[1] <- 'B';
	verif[2] <- 'C';
	verif[3] <- 'D';
	verif[4] <- 'F';
	Escribir 'Ingrese calificaion: ';
	Leer ca;
	//Definir P
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		si verif[i]=MAYUSCULAS(ca) Entonces
			p <- i;
		FinSi
	FinPara
	si p > -1 & p < 5 Entonces
		Segun p Hacer
			0: res <- "Excelente";
			1: res <- "Buena";
			2: res <- "Regular";
			3: res <- "Suficiente";
			4: res <- "Insuficiente";
		FinSegun
		Escribir " Calificacion ",res, ".";
	SiNo
		Escribir "Calificacion fuera de rango.";
	FinSi
FinProceso
