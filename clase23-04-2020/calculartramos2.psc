Proceso calcularTramos
	//Declaracion de variables
	Definir c,patron_val,patron_ret,i Como Entero;
	Definir t,tramo Como Real;
	Definir res,patron_tag Como Cadena;
	Dimension patron_val[4],patron_tag[4],patron_ret[4];
	t <- 0;
	c <- 0;
	
	//Definir patron
	patron_val[0] <- 86400; patron_tag[0] <- "d";
	patron_val[1] <- 3600; patron_tag[1] <- "h";
	patron_val[2] <- 60; patron_tag[2] <- "m";
	patron_val[3] <- 1; patron_tag[3] <- "s";
	res <- "  Tiempo total del viaje: ";
	
	//Inicio
	Escribir "Calculadora de tiempo. Ingrese intervalos Tn";
	
	//Inicia carga
	Repetir
		Escribir "t",c+1,"[min]:";
		Leer tramo;
		t <- t + tramo;
		c <- c + 1;
	Hasta Que tramo = 0;
	
	//Pasar a segundos
	t <- REDON((TRUNC(t) * 60) + (60*(t-TRUNC(t))));
	
	//Inicia proceso
	Para i<-0 Hasta 3 Con Paso 1 Hacer
		Si t >= patron_val[i] Entonces
			patron_ret[i] <- 0;
			Repetir
				patron_ret[i] <- patron_ret[i] + 1;
				t <- t - patron_val[i];
			Hasta Que t < patron_val[i];
			res <- CONCATENAR(res,CONVERTIRATEXTO(patron_ret[i]));
			res <- CONCATENAR(res,patron_tag[i]);
			res <- CONCATENAR(res," ");
		FinSi
	FinPara
	Escribir res;
FinProceso
