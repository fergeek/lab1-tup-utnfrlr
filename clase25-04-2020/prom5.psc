Proceso promedio5
	Definir vec,i,ac,max,min,dim Como Entero;
	Definir prom como Real;
	dim <- 5; ac <- 0;
	Dimension  vec[999];
	//Carga
	Para i<-0 Hasta dim - 1 Con Paso 1 Hacer
		Escribir "Ingrese valor ",i+1,"/",dim;
		Leer vec[i];
	FinPara
	//Proceso
	max <- vec[0]; min <- vec[0];
	Para i<-0 Hasta dim - 1 Hacer
		ac <- ac + vec[i];
		si vec[i] > max Entonces
			max <- vec[i];
		FinSi
		si vec[i] < min Entonces
			min <- vec[i];
		FinSi
	FinPara
	prom <- ac/dim;
	//Resultado
	Escribir "---------------------";
	Escribir "Max: ",max;
	Escribir "Min: ",min;
	Escribir "Promedio: ",prom;
	Escribir "---------------------";
FinProceso
