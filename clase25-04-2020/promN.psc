Proceso promedio5
	Definir como vec,ac,max,min real;
	Definir i,dim Como Entero;
	Definir prom como Real;
	ac <- 0;
	Dimension  vec[100];
	//Carga
	Escribir "Ingrese cantidad de elementos: ";
	Leer dim;
	Para i<-0 Hasta dim - 1 Con Paso 1 Hacer
		Escribir "Ingrese valor ",i+1,"/",dim;
		Leer vec[i];
	FinPara
	//Proceso
	max <- vec[0]; min <- vec[0];
	Para i<-0 Hasta dim - 1 Hacer
		ac <- ac + vec[i];
		si vec[i] > max Entonces
			max <- vec[i];
		FinSi
		si vec[i] < min Entonces
			min <- vec[i];
		FinSi
	FinPara
	prom <- ac/dim;
	//Resultado
	Escribir "---------------------";
	Escribir "Max: ",max;
	Escribir "Min: ",min;
	Escribir "Promedio: ",prom;
	Escribir "---------------------";
FinProceso
