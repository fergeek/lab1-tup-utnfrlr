Proceso matrizTranspuesta
	Definir A,T como Real;
	Definir dim,i,j Como Entero;
	Dimension A[100,100],T[100,100];
	//Cargar 
	Escribir "Ingrese dimension de la Matriz A (Maximo 100):";
	Leer dim;
	Para i<-0 Hasta dim - 1 Hacer
		Para j<-0 Hasta dim - 1 Hacer
			Escribir "Ingrese A",i,j,":";
			Leer A[i,j];
		FinPara
	FinPara
	//Transponer
	Para i<-0 Hasta dim - 1 Hacer
		Para j<-0 Hasta dim - 1 Hacer
			T[i,j] <- A[j,i];
		FinPara
	FinPara
	Escribir "Matriz A:";
	Para i<-0 Hasta dim - 1 Hacer
		Para j<-0 Hasta dim - 1 Hacer
			Escribir Sin Saltar A[i,j]," ";
		FinPara
		Escribir "";
	FinPara
	Escribir "Matriz trans(A):";
	Para i<-0 Hasta dim - 1 Hacer
		Para j<-0 Hasta dim - 1 Hacer
			Escribir Sin Saltar T[i,j]," ";
		FinPara
		Escribir "";
	FinPara
FinProceso
