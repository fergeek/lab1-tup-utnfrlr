//7 - Un viajero desea saber cu�nto tiempo tom� un viaje que realiz�. �l tiene la duraci�n en minutos
//de cada uno de los tramos del viaje.
//Desarrolle un programa que permita ingresar los tiempos de viaje de los tramos y entregue como
//resultado el tiempo total de viaje en formato horas:minutos.
//	El programa deja de pedir tiempos de viaje cuando se ingresa un 0.
//Duraci�n tramo: 15
//Duraci�n tramo: 30
//Duraci�n tramo: 87
//Duraci�n tramo: 0
//Tiempo total de viaje: 2:12 horas
Proceso calcularTramos
	Definir c,h,m,s,d Como Entero;
	Definir t,ac Como Real;
	Definir res Como Cadena;
	ac <- 0; c <- 0; res <- ""; s <- 0;m <- 0; h <- 0; d <- 0;
	Escribir "Este programa calcula el tiempo total de un viaje. Ingrese el tiempo de cada tramo expresado en minutos.";
	Repetir
		Escribir "Ingrese t[min] del tramo ",c+1,": ";
		Leer t;
		ac <- ac + t;
		c <- c + 1;
	Hasta Que t = 0;
	ac <- REDON(TRUNC(ac) * 60 + (60*(ac-TRUNC(ac))));
	Escribir "t = ", ac,"[seg]";
	t <- ac;
	//Dias
	Si t >= 86400 Entonces
		Repetir
			d <- d + 1;
			t <- t - 86400;
		Hasta Que t < 86400;
		res <- CONCATENAR(res,CONVERTIRATEXTO(d));
		res <- CONCATENAR(res,"dias ");
	FinSi
	//Horas
	Si t >= 3600 Entonces
		Repetir
			h <- h + 1;
			t <- t - 3600;
		Hasta Que t < 3600;
		res <- CONCATENAR(res,CONVERTIRATEXTO(h));
		res <- CONCATENAR(res,"hs ");
	FinSi
	//Minutos
	Si t >= 60 Entonces
		Repetir
			m <- m + 1;
			t <- t - 60;
		Hasta Que t < 60;
		res <- CONCATENAR(res,CONVERTIRATEXTO(m));
		res <- CONCATENAR(res,"min ");
	FinSi
	s <- t;
	res <- CONCATENAR(res,CONVERTIRATEXTO(s));
	res <- CONCATENAR(res,"seg ");	//Escribir d,"d ",h,"hs ",m,"m ",s,"s";
	Escribir res;
FinProceso
