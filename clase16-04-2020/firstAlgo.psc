Algoritmo nota
	Escribir "Ingrese nota: "
	Leer n
	Segun n Hacer
		1:
			nom = "Uno"
		2:
			nom = "Dos"
		3:
			nom = "Tres"
		4:
			nom = "Cuatro"
		5:
			nom = "Cinco"
		6:
			nom = "Seis"
		7:
			nom = "Siete"
		8:
			nom = "Ocho"
		9:
			nom = "Nueve"
		10:
			nom = "Diez"
		De Otro Modo:
			nom = "INVALIDA"
	FinSegun
	Escribir "La nota es: ",nom, "(",n,")"
FinAlgoritmo

