Proceso riesgoEnfermedadCoronaria
	definir imc,altura,peso Como Real;
	definir edad Como Entero;
	definir  riesgo como Caracter;
	Escribir "Ingrese edad del paciente:";
	Leer edad;
	Escribir "Ingrese el peso del paciente en Kg:";
	Leer peso;
	Escribir "Ingrese altura en metros:";
	Leer altura;
	imc <- (peso)/(altura^2);
	si edad < 45 Entonces
		si imc < 22 Entonces
			riesgo <- "bajo";
		SiNo
			riesgo <- "medio";
		FinSi
	SiNo
		si imc < 22 Entonces
			riesgo <- "medio";
		SiNo
			riesgo <- "alto";
		FinSi
	FinSi
	Escribir "Riesgo de enfermedad coronaria: ",riesgo;
FinProceso
