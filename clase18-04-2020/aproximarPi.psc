Proceso aproximacionDePI
	Definir t,i,c como Entero;
	Definir ac,aPI Como Real;
	ac <- 0;
	c <- (1);
	Escribir "Ingrese la cantidad de terminos:";
	Leer t;
	Para i<-1 Hasta t*2 Con Paso 1 Hacer
		si i%2=1 Entonces
			ac <- ac + (c*(1/i));
			c <- c * (-1);
		FinSi
	FinPara
	aPI <- 4 * ac;
	Escribir "PI es aproximadamente igual a: ", aPI;
FinProceso
