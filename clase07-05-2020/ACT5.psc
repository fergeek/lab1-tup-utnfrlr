subProceso str <- formatear(string,esp,relleno)
	Definir str como Cadena;
	Definir l,e,i como Entero;
	l <- LONGITUD(string);
	e <- CONVERTIRANUMERO(esp);
	str <- string;
	Si l != e Entonces
		Si l>e Entonces
			str <- SUBCADENA(str,0,e-2);
			str <- CONCATENAR(str,"*");
		SiNo
			i <- l;
			Mientras i != e Hacer
				str <- CONCATENAR(relleno,str);
				i <- i + 1;
			FinMientras
		FinSi
	FinSi
FinSubProceso
subProceso printMatriz(array,l,sizeI,sizeJ,separator)
	Definir i,j como Entero;
	Para i<-0 Hasta sizeI-1 Con Paso 1 Hacer
		Escribir Sin Saltar separator," ";
		Para j<-0 Hasta sizeJ-1 Con Paso 1 Hacer
			Escribir Sin Saltar formatear(convertirATexto(array[i,j]),convertirATexto(l)," ");
		FinPara
		Escribir " ",separator;
	FinPara
	Escribir "";
FinSubProceso
Proceso matrizAleatoria
	Definir j,i,f,m,n Como Entero;
	Definir mat,aux,rep como Real;
	m<-0;n<-9;
	dimension mat[20,20],rep[21,2];
	Para i<-0 Hasta n-1 Con Paso 1 Hacer
		Para j<-0 Hasta n-1 Con Paso 1 Hacer
			mat[i,j] <- Aleatorio(0,20);
		FinPara
	FinPara
	///MOSTRAR ARREGLO
	printMatriz(mat,3,n,n,"");
	Repetir
		Repetir
			Escribir "Desea modificar la matriz? 1-Si | 0-No";	
			Leer f;
		Hasta Que f=1 || f=0
		Si f!=0 Entonces
			Escribir Sin Saltar "Ingrese fila seguido de columna";
			Leer i,j;
			Escribir sin saltar "mat[",i,",",j,"] (",mat[i-1,j-1],") = ";
			Leer aux;
			mat[i-1,j-1] <- aux;
			m<-1;
		FinSi
	Hasta Que f=0
	///MOSTRAR ARREGLO
	printMatriz(mat,3,n,n,"");
	//RepeatTest
	
FinProceso
