Proceso promedio5
	Definir vec,i,ac,max,min Como Entero;
	Definir prom como Real;
	ac <- 0;
	Dimension  vec[5];
	//Carga
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Escribir "Ingrese valor ",i+1,"/5";
		Leer vec[i];
	FinPara
	//Proceso
	max <- vec[0]; min <- vec[0];
	Para i<-0 Hasta 4 Hacer
		ac <- ac + vec[i];
		si vec[i] > max Entonces
			max <- vec[i];
		FinSi
		si vec[i] < min Entonces
			min <- vec[i];
		FinSi
	FinPara
	prom <- ac/5;
	//Resultado
	Escribir "---------------------";
	Escribir "Max: ",max;
	Escribir "Min: ",min;
	Escribir "Promedio: ",prom;
	Escribir "---------------------";
FinProceso
