Proceso dmas
	//Declaracion de variables
	Definir c,patron_val,patron_ret,i Como Entero;
	Definir t,tramo Como Real;
	Definir res,patron_tag Como Cadena;
	Dimension patron_val[4],patron_tag[4],patron_ret[4];
	t <- 0;
	c <- 0;
	
	//Definir patron
	patron_val[0] <- 365; patron_tag[0] <- " a�o/s";
	patron_val[1] <- 30; patron_tag[1] <- " mes/es";
	patron_val[2] <- 7; patron_tag[2] <- " semana/s y";
	patron_val[3] <- 1; patron_tag[3] <- " dia/s";
	Escribir "Ingrese cantidad de dias";
	Leer t;
	res <- CONCATENAR(CONVERTIRATEXTO(t),"  Dias es equivalente a: ");
	Para i<-0 Hasta 3 Con Paso 1 Hacer
		Si t >= patron_val[i] Entonces
			patron_ret[i] <- 0;
			Repetir
				patron_ret[i] <- patron_ret[i] + 1;
				t <- t - patron_val[i];
			Hasta Que t < patron_val[i];
			res <- CONCATENAR(res,CONVERTIRATEXTO(patron_ret[i]));
			res <- CONCATENAR(res,patron_tag[i]);
			res <- CONCATENAR(res," ");
		FinSi
	FinPara
	Escribir res;
FinProceso
