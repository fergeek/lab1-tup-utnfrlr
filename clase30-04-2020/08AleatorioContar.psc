Proceso ret
	Definir vec,i,p,n,c como Entero;
	Dimension  vec[20];
	p <- 0; n <- 0; c <- 0;
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		vec[i] <- Aleatorio(-10,10);
	FinPara
	Escribir "Arreglo aleatorio: ";
	Escribir Sin Saltar "{";
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		Escribir Sin Saltar vec[i];
		Si i != 19 Entonces
			Escribir Sin Saltar ", ";
		FinSi
		Si vec[i] != 0 Entonces
			Si vec[i] > 0 Entonces
				p <- p + 1;
			SiNo
				n <- n + 1;
			FinSi
		SiNo
			c <- c+1;
		FinSi
	FinPara
	Escribir "}";
	Escribir "-----------------------------------------";
	Escribir  "Num. positivos: ",p;
	Escribir  "Num. negativos: ",n;
	Escribir "Cant. de veces que se repite 0: ",c;
	Escribir "-----------------------------------------";
FinProceso
