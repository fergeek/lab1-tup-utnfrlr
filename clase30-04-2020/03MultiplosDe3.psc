Proceso multiplo3
	Definir n,i Como Entero;
	Repetir
		Escribir "Ingrese un numero: ";
		Leer n;
		Si n < 3 Entonces
			Escribir "Error...";
		FinSi
	Hasta Que n >= 3
	Escribir "Mutiplos de 3 menores o iguales que : ",n;
	Escribir Sin Saltar "{";
	Para i<-1 Hasta n Con Paso 1 Hacer
		Si i%3 = 0 Entonces 
			Escribir sin saltar i;
			si i != n Entonces
				Escribir Sin Saltar ", ";
			FinSi
		FinSi
	FinPara
	Escribir "}";
FinProceso
