Proceso ret
	Definir vec,i como Entero;
	Dimension  vec[20];
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		vec[i] <- Aleatorio(-10,10);
	FinPara
	Escribir "Arreglo aleatorio: ";
	Escribir Sin Saltar "{";
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		Escribir Sin Saltar vec[i];
		Si i != 19 Entonces
			Escribir Sin Saltar ", ";
		FinSi
	FinPara
	Escribir "}";
FinProceso
