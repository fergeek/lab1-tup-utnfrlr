Proceso matrizIdentidad
	Definir n,i,j,k,mat Como Entero;
	Dimension mat[999,999];
	Escribir "Ingrese dimension de la matriz identidad:";
	Leer n;
	k <- 0;
	Para i<-0 Hasta n-1 Con Paso 1 Hacer
		Escribir Sin Saltar "   ";
		Para j<-0 Hasta n-1 Con Paso 1 Hacer
			si k%(n+1) = 0 Entonces
				Escribir Sin Saltar "1 ";
			SiNo
				Escribir  Sin Saltar "0 ";
			FinSi
			k <- k + 1;
		FinPara
		Escribir "";
	FinPara
FinProceso
