Proceso diaDeLaSemana
	Definir dia Como Entero;
	Definir diaN como Cadena;
	Repetir
		Escribir "Ingrese un valor:";
		Leer dia;
		Si dia < 1 o dia > 7 Entonces
			Escribir "Error intente nuevamente...";
		FinSi
	Hasta Que dia >= 1 y dia < 8
	Segun dia Hacer
		1:
			diaN <- "Lunes";
		2:
			diaN <- "Martes";
		3:
			diaN <- "Miercoles";
		4:
			diaN <- "Jueves";
		5:
			diaN <- "Viernes";
		6:
			diaN <- "Sabado";
		7:
			diaN <- "Domingo";
	FinSegun
	Escribir "El dia es: ", diaN;
FinProceso
