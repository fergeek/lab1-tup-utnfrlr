Proceso long
	Definir i Como Entero;
	Definir max,min,vec como Cadena;
	Dimension vec[5];
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Escribir "Ingrese palabra ",i+1,"/5:";
		Leer vec[i];
	FinPara
	min <- vec[0]; max <- vec[0];
	Escribir "------------------------------------------";
	Escribir "Palabras";
	Escribir "------------------------------------------";
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Escribir "Palabra[",i+1,"]: ",vec[i];
		Si LONGITUD(vec[i]) > LONGITUD(max) Entonces
			max <- vec[i];
		FinSi
		Si LONGITUD(vec[i]) < LONGITUD(min) Entonces
			min <- vec[i];
		FinSi
	FinPara
	Escribir "------------------------------------------";
	Escribir "Palabra mas larga: ",max,"[",LONGITUD(max),"]";
	Escribir "------------------------------------------";
	Escribir "Palabra mas larga: ",min,"[",LONGITUD(min),"]";
	Escribir "------------------------------------------";
FinProceso
