Proceso arraySort
	Definir i,ord,c,n como Entero;
	Definir vec, aux como Real;
	Dimension vec[999];
	n <- 10; ord <- 0;
	Escribir "Ingrese lista de valores: ";
	Para i<-0 Hasta n-1 Con Paso 1 Hacer
		Escribir "Valor ",i+1,"/",n," : ";
		Leer vec[i];
	FinPara
	Escribir "Vector:";
	Para i<-0 Hasta n-1 Con Paso 1 Hacer
		Escribir Sin Saltar " ",vec[i];
	FinPara
	Escribir "";
	Mientras ord=0 Hacer
		c <- 0;
		Para i<-1 Hasta n-1 Con Paso 1 Hacer
			Si vec[i] < vec[i-1] Entonces
					aux <- vec[i];
				//Reasignar
					vec[i] <- vec[i-1];
					vec[i-1] <- aux;
			SiNo
				c <- c + 1;
			FinSi
		FinPara
		Si c = n-1 Entonces
			ord <- 1;
		FinSi
	FinMientras
	Escribir "Vector Ordenado:";
	Para i<-0 Hasta n-1 Con Paso 1 Hacer
		Escribir Sin Saltar " ",vec[i];
	FinPara
	Escribir "";
FinProceso
