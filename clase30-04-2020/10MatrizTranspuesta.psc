subProceso str <- formatear(string,esp,relleno)
	Definir str como Cadena;
	Definir l,e,i,ord como Entero;
	l <- LONGITUD(string);
	e <- CONVERTIRANUMERO(esp);
	str <- string;
	Si l != e Entonces
		Si l>e Entonces
			str <- SUBCADENA(str,0,e-2);
			str <- CONCATENAR(str,"*");
		SiNo
			i <- l;
			Mientras i != e Hacer
				str <- CONCATENAR(relleno,str);
				i <- i + 1;
			FinMientras
		FinSi
	FinSi
FinSubProceso
Proceso matrizTranspuesta
	Definir mat,trans Como Real;
	Definir i,j,n Como Entero;
	n <- 3;
	Dimension mat[999,999],trans[999,999];
	Escribir "Ingrese una matriz A ",n,"x",n;
	Para i<-0 Hasta n-1 Con Paso 1 Hacer
		Para j<-0 Hasta n-1 Con Paso 1 Hacer
			Escribir "ingrese A",i+1,j+1,": ";
			Leer mat[i,j];
			trans[j,i] <- mat[i,j];
		FinPara
	FinPara
	Escribir "";
	Escribir " Matriz A";
	Escribir "";
	Para i<-0 Hasta n-1 Con Paso 1 Hacer
		Para j<-0 Hasta n-1 Con Paso 1 Hacer
			Escribir Sin Saltar formatear(ConvertirATexto(mat[i,j]),"5"," ");
		FinPara
		Escribir "";
	FinPara
	Escribir "";
	Escribir " Matriz A transpuesta";
	Escribir "";
	Para i<-0 Hasta n-1 Con Paso 1 Hacer
		Para j<-0 Hasta n-1 Con Paso 1 Hacer
			Escribir Sin Saltar formatear(ConvertirATexto(trans[i,j]),"5"," ");
		FinPara
		Escribir "";
	FinPara
FinProceso
